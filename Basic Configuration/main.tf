
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.72.0"
    }
  }
}

provider "azurerm" {
  tenant_id       = "17e32440-c4c6-487c-a361-41272e6db574"
  subscription_id = "8849218e-7382-42ab-9434-352bfc1e1c68"
  client_id       = "750d8ec5-2538-4168-8dba-d6665886ebf4"
  client_secret   = "TEIpitVAVLI_CcjAA2yyVZWy-MmUw8lb_1"
  features {}
}

resource "azurerm_resource_group" "_tf_rg_az-fd-prod-1" {
    location = "Eastus"
    name     = "az-fd-prod-1"
    tags     = {
        "Environment" = "Production"
        "Owner"       = "DevOps Sergey Sakharov"
        "Resource"    = "Managed by Terraform"
        "b2b-role"    = "frontdoor-prod-1"
    }
}

resource "azurerm_resource_group" "_tf_rg_az-fd-prod-2" {
    location = "Eastus"
    name     = "az-fd-prod-2"
    tags     = {
        "Environment" = "Production"
        "Owner"       = "DevOps Sergey Sakharov"
        "Resource"    = "Managed by Terraform"
        "b2b-role"    = "frontdoor-prod-2"
    }
}

resource "azurerm_resource_group" "_tf_rg_az-fd-prod-3" {
    location = "Eastus"
    name     = "az-fd-prod-3"
    tags     = {
        "Environment" = "Laboratory"
        "Owner"       = "DevOps Sergey Sakharov"
        "Resource"    = "Managed by Terraform"
        "b2b-role"    = "frontdoor-prod-3"
    }
}

resource "azurerm_resource_group" "_tf_rg_az-kuber-1" {
    location = "Eastus"
    name     = "az-kuber-1"
    tags     = {
        "Environment" = "Laboratory"
        "Owner"       = "DevOps Sergey Sakharov"
        "Resource"    = "Managed by Terraform"
        "b2b-role"    = "Kuber-1"
    }
}

  
resource "azurerm_virtual_network" "_tf_vnet_az-fd-prod-3" {
  name                = "az-fd-prod-3-net-1"
  location            = azurerm_resource_group._tf_rg_az-fd-prod-3.location
  resource_group_name = azurerm_resource_group._tf_rg_az-fd-prod-3.name
  address_space       = ["10.0.0.0/16"]
  dns_servers         = ["10.0.0.4", "10.0.0.5"]

 # ddos_protection_plan {
 #   id     = azurerm_network_ddos_protection_plan.az-fd-prod-3.id
 #   enable = true
 # }

  subnet {
    name           = "az-fd-prod-3-net-1-subent-1-xxx"
    address_prefix = "10.0.1.0/24"
  }

  subnet {
    name           = "az-fd-prod-3-net-1-subent-2-xxx"
    address_prefix = "10.0.2.0/24"
  }

  subnet {
    name           = "az-fd-prod-3-net-1-subent-3-xxx"
    address_prefix = "10.0.3.0/24"
  #  security_group = azurerm_network_security_group.example.id
  }

  tags = {
    environment = "Laboratory"
  }
}

#frommypc
